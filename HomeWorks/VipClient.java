package com.company;

import java.util.Arrays;

public class VipClient extends Client {

    //Изменения вносить только в этот класс!!!

    /**
     * Задачи:
     *  1. Переопределить метод getDiscount - скидка зависит от рейтинга клиента (каждая 1000 = 1%), если есть хотя бы 1 заказ
     *  дополнительно +5% скидки
     *  2. Переопределить метод comparateTo - сравнивать клиентов нужно по рейтингу
     *  Пример c1.compareTo(c2) метод возвращает 1 если рейтинг c1 > c2, -1 если рейтинг c2 > c1 и 0 если рейтинг с2 = с1
     *  3. Переопределить метод toString - в конце вывода нужно добавить строку "Рейтинг: N"
     *
     * После выполнения заданий в консоли должен вывестись следующий результат
     *
     *** VIP Клиенты ***

     Клиент: John Travolta (тел. 47-905-025483)
     Заказы: [12410101, 09184911]
     Рейтинг: 7100 (12.0%)
     =======================
     Клиент: Bruce Willis (тел. 870-442-04279)
     Заказы: []
     Рейтинг: 9000 (9.0%)
     =======================
     Клиент: Samuel L. Jackson (тел. 55-42-06726)
     Заказы: [91810101, 75192211]
     Рейтинг: 12500 (17.0%)
     =======================
     */

    private int rating; // рейтинг клиента

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public double getDiscount() {
        double discont = 0.0;
        int localCopyRating = rating;
        if (orderIds.length >=1) {
            discont += 5;
        }
        for (;localCopyRating >=1000;localCopyRating-=1000){
            discont++;
        }
        return discont;

    }

    public int compareTo(Client client) {
        VipClient client1 = (VipClient)client;
        int res = 0;
        if (rating>client1.rating)res=1;
        if (rating<client1.rating)res=-1;
        return res;
    }

    public String toString() {
        return "Клиент: " + name + " (тел. " + phone + ")" + "\n"
                + "Заказы: " + Arrays.asList(orderIds).toString() + "\n" + "Рейтенг: " + rating;
    }
}
